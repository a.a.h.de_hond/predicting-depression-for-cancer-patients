# Predicting depression risk for cancer patients with deep Neural Networks using EHR data, patient emails, and clinical notes
***
## Name
"Predicting depression risk for cancer patients with deep Neural Networks using EHR data, patient emails, and clinical notes" - code written by Anne de Hond and Claudio Fanconi 

## Description
This repository contains code to train machine learning models and deep language models to predict depression risk for cancer patients within a month of starting treatment.

## Installation
Clone the current repository
```
git clone https://gitlab.com/a.a.h.de_hond/predicting-depression-for-cancer-patients.git
```

For the language models, we suggest to create a virtual environment and install the required packages:
```
conda create --name acu python=3.8
conda activate acu
conda install pytorch cudatoolkit=11.1 -c pytorch -c nvidia
conda install -r 'NLP models/requirements.txt'
```

### Source Code Directory Tree
```
ML models               # Training notebooks ML models
NLP models
└── src                 # Source code NLP models          
    ├── layers              # Single Neural Network layers
    ├── model               # Neural Network Models for NLP
    ├── data                # Folder with data processing parts
    └── utils               # Useful functions, such as loggers and config 
```


## Authors
Anne de Hond - ML models (a.a.h.de_hond@lumc.nl, anne.dehond93@gmail.com)
Claudio Fanconi - NLP models (fanconic@ethz.ch, fanconic@gmail.com)


## Project status
Submitted scientific publication
